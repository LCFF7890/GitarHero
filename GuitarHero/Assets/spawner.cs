﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawner : MonoBehaviour
{
    public GameObject spawner1;
    public GameObject spawner2;
    public GameObject spawner3;
    public GameObject spawner4;
    public GameObject spawner5;
    public GameObject Note;
    private float nextNote = 0.0F;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        switch (Random.Range(0, 5))
        {
            case 0:
                if (Time.time > nextNote)
                {
                    nextNote = Random.Range(0.5f, 2) + Time.time;
                    Salir(spawner1);
                }
                break;
            case 1:
                if (Time.time > nextNote)
                {
                    nextNote = Random.Range(0.5f, 2) + Time.time;
                    Salir(spawner2);
                }
                break;
            case 2:
                if (Time.time > nextNote)
                {
                    nextNote = Random.Range(0.5f, 2) + Time.time;
                    Salir(spawner3);
                }
                break;
            case 3:
                if (Time.time > nextNote)
                {
                    nextNote = Random.Range(0.5f, 2) + Time.time;
                    Salir(spawner4);
                }
                break;
            case 4:
                if (Time.time > nextNote)
                {
                    nextNote = Random.Range(0.5f, 2) + Time.time;
                    Salir(spawner5);
                }
                break;
        }
    }
    void Salir(GameObject spawner)
    {
        GameObject instNote = Instantiate(Note, spawner.transform.position, Quaternion.identity);
        Rigidbody instNoteRig = instNote.GetComponent<Rigidbody>();
        instNoteRig.AddForce(Vector3.forward * -500f);

    }
}
